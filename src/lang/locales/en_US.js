/* Table of Contents */

// 01. Menu
// 02. Pages
// 03. Forms

module.exports = {
  /* 01. Menu */
  'menu.home': 'Home',
  'menu.blank-page': 'Blank Page',
  'menu.home-page': 'Home',
  'menu.all-videos': 'Videos',
  'menu.all-videos-page': 'Videos',
  'menu.production-house-page': 'PH',
  'menu.actress': 'Actress',
  'menu.actress-page': 'Actress',
  'menu.settings-page': 'Settings',

  /* 02. Pages */
  'pages.error-title': 'Whoopsies',
  'pages.error-code': 'Error Code',
  'pages.unauthorized-title': 'Unauthorized',
  'pages.unauthorized-detail': 'You are not authorized!',
  'pages.go-back-home': 'Go Back Home',

  /* 03. Forms */
  'forms.update': 'Update',
};
