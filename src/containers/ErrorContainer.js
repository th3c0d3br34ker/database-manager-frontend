import React, { useEffect } from 'react';
import { Card, CardBody, CardTitle, Row } from 'reactstrap';
import { Colxx } from '../components/common/CustomBootstrap';
import { adminRoot } from '../constants/defaultValues';
import IntlMessages from '../helpers/IntlMessages';

const ErrorContainer = ({ name, message }) => {
  useEffect(() => {
    document.body.classList.add('background');
    document.body.classList.add('no-footer');

    return () => {
      document.body.classList.remove('background');
      document.body.classList.remove('no-footer');
    };
  }, []);

  return (
    <>
      <div className="fixed-background" />
      <main>
        <div className="container">
          <Row className="h-100">
            <Colxx xxs="12" md="8" className="mx-auto my-auto">
              <Card>
                <CardBody className="text-center">
                  <CardTitle className="mb-4">
                    <IntlMessages id="pages.error-title" />
                  </CardTitle>

                  <h1 className="lead">{`${name}: ${message}`}</h1>
                  {/* <p>{message}</p> */}

                  <p className="mb-0 text-muted text-small mb-0">
                    <IntlMessages id="pages.error-code" />
                  </p>
                  <p className="display-1 font-weight-bold mb-5">404</p>
                  <a
                    href={adminRoot}
                    className="btn btn-primary btn-shadow btn-lg"
                  >
                    <IntlMessages id="pages.go-back-home" />
                  </a>
                </CardBody>
              </Card>
            </Colxx>
          </Row>
        </div>
      </main>
    </>
  );
};

export default ErrorContainer;
