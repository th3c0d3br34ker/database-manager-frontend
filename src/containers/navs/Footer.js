/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect, useState } from 'react';
import { Row } from 'reactstrap';
import { Colxx } from '../../components/common/CustomBootstrap';
import AppError from '../../helpers/AppError';
import getData from '../../helpers/get-data';

const renderStatus = (status) => {
  if (status === 'OK') {
    return <i className="simple-icon-check mr-2 text-success" />;
  }

  return <i className="simple-icon-exclamation text-danger" />;
};

const getAPI = async () => {
  const response = await getData('/', '__api_status');

  if (response.success) {
    return response.data;
  }
  throw new AppError('Unable to reach the API!');
};

const Footer = () => {
  const [status, setStatus] = useState('NO');

  useEffect(() => {
    getAPI().then((newStatus) => setStatus(newStatus));
  }, []);

  return (
    <footer className="page-footer">
      <div className="footer-content">
        <div className="container-fluid">
          <Row>
            <Colxx xxs="12" md="10">
              <p className="mb-0 text-muted">Database Manager © 2021</p>
            </Colxx>

            <Colxx xx="12" md="2" className="col-md-2 d-md-block">
              <p className="mb-0 text-muted ">
                {`API Status  `}
                {renderStatus(status)}
              </p>
            </Colxx>
          </Row>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
