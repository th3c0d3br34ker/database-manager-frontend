/* eslint-disable react/self-closing-comp */
/* eslint-disable jsx-a11y/media-has-caption */
/* eslint-disable camelcase */

import React from 'react';
import { Button, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import {
  Player,
  ControlBar,
  LoadingSpinner,
  PlaybackRateMenuButton,
  BigPlayButton,
} from 'video-react';
import { getSettings } from '../../helpers/Utils';

const { videoUri } = getSettings();

const VideoPlayModal = ({ setOpenVideoPlayModal, data }) => {
  const { name, series, actress, file_path } = data;

  return (
    <>
      <ModalHeader>
        <p className="lead mb-0">{series}</p>
        <p className="mb-2">{name}</p>
        <p className="mb-2">{actress.join(', ')}</p>
      </ModalHeader>
      <ModalBody>
        <Player
          className="embed-responsive-item"
          height="400px"
          src={`${videoUri}/${file_path}`}
          title={name}
          fluid
          muted
        >
          <ControlBar autoHide={false}>
            <PlaybackRateMenuButton
              rates={[5, 2, 1.75, 1.5, 1, 0.5, 0.1]}
              order={7.1}
            />
          </ControlBar>
          <LoadingSpinner />
          <BigPlayButton position="center" />
        </Player>
      </ModalBody>
      <ModalFooter className="justify-content-end">
        <Button color="danger" onClick={() => setOpenVideoPlayModal(false)}>
          Close
        </Button>
      </ModalFooter>
    </>
  );
};

export default VideoPlayModal;
