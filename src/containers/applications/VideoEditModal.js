/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable camelcase */
import React, { useState } from 'react';
import { Badge, Button, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Separator } from '../../components/common/CustomBootstrap';
import StateButton from '../../StateButton';

import updateData from '../../helpers/update-data';

const VideoEditModal = ({ setOpenVideoEditModal, data: videoData }) => {
  const { _id, name, actress, series, publication_date } = videoData;

  const [formValues, setFormValues] = useState(videoData);

  const handleUpdateVideo = (newFormValues) => {
    const promise = new Promise((resolve, reject) => {
      const callUpdate = async () => {
        const response = await updateData('/update-data', _id, newFormValues);

        if (response.success) {
          resolve('Data updated successfully!');
        } else {
          reject('Failed to update data!');
        }
      };

      callUpdate(resolve, reject);
    });

    return promise;
  };

  const loading = false;

  const date = new Date(publication_date);
  return (
    <>
      {name && (
        <ModalHeader>
          <p className="lead">{name}</p>
        </ModalHeader>
      )}

      <ModalBody>
        {loading ? (
          <div className="loading" />
        ) : (
          <>
            <p className="m-1 h5">Series </p>
            <p className="m-1">{series}</p>
            <Separator className="mb-4" />

            <p className="m-1 54">Actress</p>
            <p className="m-1">{actress.join(', ')}</p>
            <Separator className="mb-4" />

            <p className="m-1 h5">Publication Date </p>
            <p className="m-1">{`${date.toDateString()}`}</p>
            <Separator className="mb-4" />

            <p className="m-1 h5">ID </p>
            <p className="m-1">{_id}</p>
            <Separator className="mb-4" />

            <h4 className="m-2 h5">Starred </h4>
            <Badge
              color={`${formValues.starred ? 'success' : 'danger'}`}
              pill
              className="m-1"
              onClick={() => {
                setFormValues({
                  ...formValues,
                  starred: !formValues.starred,
                });
              }}
            >
              <i className="simple-icon-star" />
            </Badge>

            <Separator className="mb-4" />
          </>
        )}
      </ModalBody>

      <ModalFooter>
        <StateButton
          id="updateButton"
          color="secondary"
          onClick={() => handleUpdateVideo(formValues)}
        >
          Update
        </StateButton>
        <Button color="danger" onClick={() => setOpenVideoEditModal(false)}>
          Close
        </Button>
      </ModalFooter>
    </>
  );
};

export default VideoEditModal;
