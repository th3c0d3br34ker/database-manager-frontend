/* eslint-disable prefer-promise-reject-errors */
import React, { useState } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  FormGroup,
  Input,
  Row,
} from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import StateButton from '../../../components/StateButton';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import getData from '../../../helpers/get-data';
import IntlMessages from '../../../helpers/IntlMessages';
import updateData from '../../../helpers/update-data';
import { getSettings, setSettings } from '../../../helpers/Utils';

const settings = getSettings();
const env = 'dev';

const handleChangeSettings = async (newData) => {
  return new Promise((success, fail) => {
    const updateSettings = async () => {
      const response = await updateData('/settings', '', {
        env: 'dev',
        ...newData,
      });

      if (response.success) {
        setSettings(response.data);
        success('Settings Updated Successfully!');
      } else {
        fail('Failed to Update Settings!');
      }
    };

    updateSettings();
  });
};

const handleSyncSettings = async () => {
  return new Promise((success, fail) => {
    try {
      const updateSettings = async () => {
        const response = await getData(`/settings?env=${env}`);

        if (response.success) {
          setSettings(response.data);
          success('Settings Updated Successfully!');
        } else {
          fail('Failed to Update Settings!');
        }
      };

      updateSettings();
    } catch {
      fail('Failed to Update Settings!');
    }
  });
};

const SettingsPage = ({ match }) => {
  const [formValues, setFormValues] = useState({
    ...settings,
  });

  const [editSettings, setEditSettings] = useState(false);

  const handleChange = (event, key) => {
    setFormValues({
      ...formValues,
      [key]: event.target.value,
    });
  };

  return (
    <>
      <Row>
        <Colxx xxs="8">
          <Breadcrumb heading="menu.settings-page" match={match} />
        </Colxx>
        <Colxx xxs="4">
          <Button
            size="sm"
            className="m-1"
            onClick={() => {
              setSettings(null);
              window.location.reload();
            }}
          >
            <i className="simple-icon-refresh mr-2" />
            <i>Refresh</i>
          </Button>

          <Button
            size="sm"
            className="m-1"
            onClick={() => {
              handleSyncSettings();
            }}
          >
            <i className="simple-icon-refresh mr-2" />
            <i>Sync</i>
          </Button>
        </Colxx>
      </Row>

      <Separator className="mb-5" />

      <Row className="justify-content-center">
        <Colxx xxs="12" md="8">
          <Card className="mb-4">
            <div className="position-absolute card-top-buttons">
              <Button
                outline
                color="primary"
                onClick={() => {
                  setEditSettings(!editSettings);
                }}
                className="icon-button"
              >
                <i className="simple-icon-pencil" />
              </Button>
            </div>
            <CardBody>
              <CardTitle>
                <h2 className="lead">
                  <IntlMessages id="menu.settings-page" />
                </h2>
              </CardTitle>

              <FormGroup className="mb-6">
                <Input
                  type="text"
                  className="form-control"
                  name="api-uri"
                  value={formValues.apiUri}
                  onChange={(event) => {
                    handleChange(event, 'apiUri');
                  }}
                  disabled={!editSettings}
                />
              </FormGroup>

              <FormGroup className="mb-6">
                <Input
                  type="text"
                  className="form-control"
                  name="video-uri"
                  value={formValues.videoUri}
                  onChange={(event) => {
                    handleChange(event, 'videoUri');
                  }}
                  disabled={!editSettings}
                />
              </FormGroup>

              {editSettings && (
                <FormGroup className="text-center">
                  <StateButton
                    id="submitLinks"
                    color="primary"
                    type="submit"
                    onClick={() => handleChangeSettings(formValues)}
                    next={() => {
                      setEditSettings(false);
                    }}
                  >
                    <IntlMessages id="forms.update" />
                  </StateButton>
                </FormGroup>
              )}
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </>
  );
};

export default SettingsPage;
