import React from 'react';
import { Button, Row } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
// import CardView from './card-view';
import TableView from './table-view';

const HomePage = ({ match }) => {
  return (
    <>
      <Row>
        <Colxx xxs="10">
          <Breadcrumb match={match} />
        </Colxx>
        <Colxx xxs="2" className="align-items-center">
          <Button
            size="sm"
            onClick={() => {
              window.location.reload();
            }}
          >
            <i className="simple-icon-refresh mr-2" />
            <i>Refresh</i>
          </Button>
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <TableView />
      </Row>
    </>
  );
};

export default HomePage;
