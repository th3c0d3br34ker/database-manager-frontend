/* eslint-disable react/no-array-index-key */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/display-name */

import React, { useMemo, useState } from 'react';
import { Badge, Button, Modal } from 'reactstrap';
import Table from '../../../components/DataTable';
import { Colxx } from '../../../components/common/CustomBootstrap';
import VideoEditModal from '../../../containers/applications/VideoEditModal';
import VideoPlayModal from '../../../containers/applications/VideoPlayModal';

import useFetch from '../../../hooks/use-fetch';
import AppError from '../../../helpers/AppError';

const TableView = () => {
  const { loading, data, error } = useFetch('/get-data/all', '__video_data');

  const [openVideoPlayModal, setOpenVideoPlayModal] = useState(false);
  const [openVideoEditModal, setOpenVideoEditModal] = useState(false);
  const [currentItem, setCurrentItem] = useState({});

  const cols = useMemo(
    () => [
      {
        Header: 'Series',
        accessor: 'series',
        cellClass: 'list-item-heading align-middle w-20',
        Cell: (props) => <>{props.value}</>,
      },
      {
        Header: 'Actress',
        accessor: 'actress',
        cellClass: 'align-middle w-10',
        Cell: (props) => (
          <>
            <div className="d-flex flex-column">
              {props.value.map((name, idx) => (
                <Badge
                  key={idx}
                  pill
                  color="primary"
                  className="align-self-center mb-1"
                >
                  {name}
                </Badge>
              ))}
            </div>
          </>
        ),
      },
      {
        Header: 'Name',
        accessor: 'name',
        cellClass: 'align-middle w-30',
        Cell: (props) => <>{props.value}</>,
      },
      {
        Header: '',
        accessor: 'buttons',
        cellClass: 'align-middle w-10',
        Cell: (props) => (
          <>
            <Button
              outline
              size="xs"
              color="primary"
              className="align-self-center m-1 simple-icon-info"
              onClick={() => {
                setCurrentItem(props.data[props.row.id]);
                setOpenVideoEditModal(true);
              }}
            />
            <Button
              size="xs"
              className="align-self-center m-1 simple-icon-control-play"
              color="primary"
              onClick={() => {
                setCurrentItem(props.data[props.row.id]);
                setOpenVideoPlayModal(true);
              }}
            />
          </>
        ),
      },
    ],
    []
  );

  if (loading) return <div className="loading" />;

  if (error) throw new AppError(error);

  return (
    <Colxx xxs="12">
      <div className="mb-4">
        <Table columns={cols} data={data?.videos} divided />

        <Modal
          isOpen={openVideoEditModal}
          toggle={() => setOpenVideoEditModal(!openVideoEditModal)}
          wrapClassName="modal-right"
        >
          <VideoEditModal
            setOpenVideoEditModal={setOpenVideoEditModal}
            data={currentItem}
          />
        </Modal>

        <Modal
          isOpen={openVideoPlayModal}
          size="lg"
          toggle={() => setOpenVideoPlayModal(!openVideoPlayModal)}
        >
          <VideoPlayModal
            setOpenVideoPlayModal={setOpenVideoPlayModal}
            data={currentItem}
          />
        </Modal>
      </div>
    </Colxx>
  );
};

export default TableView;
