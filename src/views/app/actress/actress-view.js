/* eslint-disable react/no-array-index-key */
/* eslint-disable react/display-name */
import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Badge } from 'reactstrap';

import Table from '../../../components/DataTable';
import { adminRoot } from '../../../constants/defaultValues';

const ShowActressData = ({ data }) => {
  const cols = useMemo(
    () => [
      {
        Header: 'Actress',
        accessor: 'actress',
        cellClass: 'list-item-heading align-middle w-20',
        Cell: ({ value }) => <>{value}</>,
      },
      {
        Header: 'Videos',
        accessor: 'videos',
        cellClass: 'align-middle w-70',
        Cell: ({ value }) => (
          <>
            {value.map((item) => (
              <Badge
                key={item.id}
                pill
                color="primary"
                className="align-self-center m-1"
              >
                {`${item.series} - ${item.name}`}
              </Badge>
            ))}
          </>
        ),
      },
      {
        Header: 'Videos',
        accessor: 'default',
        cellClass: 'align-middle w-10',
        Cell: ({ row }) => (
          <NavLink
            color="secondary"
            className="align-self-center"
            to={`${adminRoot}/actress-page/actress-video-view/${row.values.actress}`}
          >
            <i className="simple-icon-arrow-right-circle display-4" />
          </NavLink>
        ),
      },
    ],
    []
  );
  return (
    <div className="mb-4">
      <Table columns={cols} data={data} divided defaultPageSize={50} />
    </div>
  );
};

ShowActressData.propType = {
  data: PropTypes.object.isRequired,
};
export default ShowActressData;
