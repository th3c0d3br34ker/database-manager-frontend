import React from 'react';
import { Button, Row } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import AppError from '../../../helpers/AppError';
import useFetch from '../../../hooks/use-fetch';

import ShowActressData from './actress-view';

const ActressPage = ({ match }) => {
  const { loading, data, error, remove } = useFetch(
    '/actress/get-actresses',
    '__actress_data',
    true
  );

  if (loading) return <div className="loading" />;

  if (error) throw new AppError(error);

  return (
    <>
      <Row>
        <Colxx xxs="10">
          <Breadcrumb heading="menu.actress" match={match} />
        </Colxx>
        <Colxx xxs="2" className="align-items-center">
          <Button
            size="sm"
            onClick={() => {
              remove();
              window.location.reload();
            }}
          >
            <i className="simple-icon-refresh mr-2" />
            <i>Refresh</i>
          </Button>
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12">
          <ShowActressData data={data.actresses} />
          {/* <pre>{JSON.stringify(data.actresses, null, 2)}</pre> */}
        </Colxx>
      </Row>
    </>
  );
};

export default ActressPage;
