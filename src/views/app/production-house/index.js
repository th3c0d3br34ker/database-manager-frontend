/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Button, Row } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import AppError from '../../../helpers/AppError';
import useFetch from '../../../hooks/use-fetch';

import ShowPHData from './ph-view';

const ProductionHousePage = ({ match }) => {
  const { loading, data, error, remove } = useFetch(
    '/get-production-houses',
    '__ph_data'
  );

  if (loading) return <div className="loading" />;

  if (error) throw new AppError(error);

  return (
    <>
      <Row>
        <Colxx xxs="10">
          <Breadcrumb match={match} />
        </Colxx>
        <Colxx xxs="2" className="align-items-center">
          <Button
            size="sm"
            onClick={() => {
              remove();
              window.location.reload();
            }}
          >
            <i className="simple-icon-refresh mr-2" />
            <i>Refresh</i>
          </Button>
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12">
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row>
        <Colxx xx="12">
          <ShowPHData data={data.productionHouses} />
        </Colxx>
      </Row>
    </>
  );
};

export default ProductionHousePage;
