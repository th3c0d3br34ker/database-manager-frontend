/* eslint-disable react/no-array-index-key */
/* eslint-disable react/display-name */
import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Badge } from 'reactstrap';
import { NavLink } from 'react-router-dom';

import Table from '../../../components/DataTable';
import { adminRoot } from '../../../constants/defaultValues';

const ShowPHData = ({ data }) => {
  const cols = useMemo(
    () => [
      {
        Header: 'Production House',
        accessor: 'name',
        cellClass: 'list-item-heading align-middle w-25',
        Cell: ({ value }) => <>{value}</>,
      },
      {
        Header: 'Series',
        accessor: 'series',
        cellClass: 'align-middle w-75',
        Cell: ({ value, row }) => (
          <>
            {value.map((name, idx) => (
              <NavLink
                key={idx}
                className="align-self-center"
                to={`${adminRoot}/production-house-page/ph-video-view/${name.replaceAll(
                  ' ',
                  ''
                )}`}
              >
                <Badge pill color="primary" className="align-self-center m-1">
                  {name}
                </Badge>
              </NavLink>
            ))}

            {value.length === 0 && (
              <NavLink
                className="align-self-center m-1"
                to={`${adminRoot}/production-house-page/ph-video-view/${row.values.name.replaceAll(
                  ' ',
                  ''
                )}`}
              >
                <Badge pill color="primary" className="align-self-center m-1">
                  {row.values.name}
                </Badge>
              </NavLink>
            )}
          </>
        ),
      },
    ],
    []
  );
  return (
    <div className="mb-4">
      <Table columns={cols} data={data} divided defaultPageSize={10} />
    </div>
  );
};

ShowPHData.propType = {
  data: PropTypes.object.isRequired,
};
export default ShowPHData;
