import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../../layout/AppLayout';

const HomePage = React.lazy(() =>
  import(/* webpackChunkName: "home" */ './home')
);
const ProductionHousePage = React.lazy(() =>
  import(/* webpackChunkName: "production-house" */ './production-house')
);
const ProductionHouseVideoView = React.lazy(() =>
  import(
    /* webpackChunkName: "production-house-ph-video-view" */ './production-house/ph-videos-view'
  )
);
const ActressVideoView = React.lazy(() =>
  import(
    /* webpackChunkName: "production-house-ph-video-view" */ './actress/actress-videos-view'
  )
);
const ActressPage = React.lazy(() =>
  import(/* webpackChunkName: "actress-page" */ './actress')
);
const SettingsPage = React.lazy(() =>
  import(/* webpackChunkName: "settings-page" */ './settings')
);

const App = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect
              exact
              from={`${match.url}/`}
              to={`${match.url}/production-house-page`}
            />

            <Route
              path={`${match.url}/all-videos-page`}
              render={(props) => <HomePage {...props} />}
            />

            <Route
              path={`${match.url}/production-house-page/ph-video-view/:series`}
              render={(props) => <ProductionHouseVideoView {...props} />}
            />

            <Route
              path={`${match.url}/production-house-page`}
              render={(props) => <ProductionHousePage {...props} />}
            />

            <Route
              path={`${match.url}/actress-page/actress-video-view/:actress`}
              render={(props) => <ActressVideoView {...props} />}
            />

            <Route
              path={`${match.url}/actress-page`}
              render={(props) => <ActressPage {...props} />}
            />

            <Route
              path={`${match.url}/settings-page`}
              render={(props) => <SettingsPage {...props} />}
            />

            <Redirect to="/error" />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
