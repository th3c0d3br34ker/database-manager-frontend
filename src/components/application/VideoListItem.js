/* eslint-disable camelcase */
/* eslint-disable react/no-array-index-key */

import React, { useState } from 'react';
import { Badge, Button, Card, CardBody, Modal } from 'reactstrap';
import { Colxx } from '../common/CustomBootstrap';
import VideoPlayModal from '../../containers/applications/VideoPlayModal';
import VideoEditModal from '../../containers/applications/VideoEditModal';

const VideoItem = ({ item }) => {
  const { series, name, actress } = item;

  const [openVideoPlayModal, setOpenVideoPlayModal] = useState(false);
  const [openVideoEditModal, setOpenVideoEditModal] = useState(false);

  return (
    <>
      <Colxx xxs="12">
        <Card className="mb-4">
          <div className="d-flex flex-grow-1 min-width-zero">
            <CardBody className="align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
              <div className="list-item-heading w-25 w-xs-100 truncate">
                <i className="iconsminds-video-5 heading-icon" />
                <span className="align-middle">{series}</span>
              </div>

              <div className="d-flex flex-column w-20 w-xs-100">
                {actress.map((element, idx) => (
                  <Badge
                    key={idx}
                    pill
                    color="primary"
                    className="align-self-center mb-1"
                  >
                    {element}
                  </Badge>
                ))}
              </div>

              <p className="mb-1 w-45 w-xs-100">{name}</p>

              <Button
                outline
                size="xs"
                color="primary"
                className="mb-1 mr-1"
                onClick={() => setOpenVideoEditModal(true)}
              >
                Details
              </Button>

              <Button
                size="xs"
                className="mb-1 ml-1"
                color="primary"
                onClick={() => setOpenVideoPlayModal(true)}
              >
                Play
              </Button>
            </CardBody>
          </div>
        </Card>
      </Colxx>

      <Modal
        isOpen={openVideoEditModal}
        size="lg"
        toggle={() => setOpenVideoEditModal(!openVideoEditModal)}
        wrapClassName="modal-right"
      >
        <VideoEditModal
          setOpenVideoEditModal={setOpenVideoEditModal}
          data={item}
        />
      </Modal>

      <Modal
        isOpen={openVideoPlayModal}
        size="lg"
        toggle={() => setOpenVideoPlayModal(!openVideoPlayModal)}
      >
        <VideoPlayModal
          setOpenVideoPlayModal={setOpenVideoPlayModal}
          data={item}
        />
      </Modal>
    </>
  );
};

export default VideoItem;
