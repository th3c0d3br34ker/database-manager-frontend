import { adminRoot } from './defaultValues';

const data = [
  {
    id: 'production-house',
    icon: 'iconsminds-video',
    label: 'menu.production-house-page',
    to: `${adminRoot}/production-house-page`,
  },
  {
    id: 'videos',
    icon: 'iconsminds-film-video',
    label: 'menu.all-videos',
    to: `${adminRoot}/all-videos-page`,
  },
  {
    id: 'actress',
    icon: 'iconsminds-female-2',
    label: 'menu.actress',
    to: `${adminRoot}/actress-page`,
  },
  {
    id: 'settings',
    icon: 'simple-icon-settings',
    label: 'menu.settings-page',
    to: `${adminRoot}/settings-page`,
  },
  // {
  //   id: 'blankpage',
  //   icon: 'iconsminds-bucket',
  //   label: 'menu.blank-page',
  //   to: `${adminRoot}/blank-page`,
  // },
];

export default data;
