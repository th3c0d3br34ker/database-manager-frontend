import AppError from './AppError';
import instance from './axiosInstance';

const updateData = (url, id, data) => {
  return instance({
    method: 'POST',
    url: `${url}/${id}`,
    data,
  })
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      throw new AppError(err);
    });
};

export default updateData;
