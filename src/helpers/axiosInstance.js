/* eslint-disable no-console */
import axios from 'axios';
import { getSettings } from './Utils';
// import { apiUri } from '../constants/defaultValues';

const { apiUri } = getSettings();

const instance = axios.create({
  baseURL: apiUri,
  headers: {
    'Content-Type': 'application/json;charset=utf-8',
  },
  timeout: 3000,
});

instance.interceptors.request.use((request) => {
  console.log(
    '>>> Time : ',
    new Date(Date.now()).toLocaleTimeString(),
    '\n',
    'Starting Request',
    JSON.stringify(request, null, 2)
  );

  return request;
});

instance.interceptors.response.use((response) => {
  console.log(
    '>>> Time : ',
    new Date(Date.now()).toLocaleTimeString(),
    '\n',
    'Response: ',
    JSON.stringify(response, null, 2)
  );

  return response;
});

export default instance;
