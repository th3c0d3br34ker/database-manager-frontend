import AppError from './AppError';
import instance from './axiosInstance';

const getData = (url) => {
  return instance({
    method: 'GET',
    url,
  })
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      throw new AppError(err);
    });
};

export default getData;
