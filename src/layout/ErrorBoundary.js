/* eslint-disable no-unused-vars */
import React from 'react';
import { Card, CardBody, Row } from 'reactstrap';
import { Colxx } from '../components/common/CustomBootstrap';
import ErrorContainer from '../containers/ErrorContainer';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false, errorName: '', errorMessage: '' };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return {
      hasError: true,
      errorName: error.name,
      errorMessage: error.message,
    };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    // console.trace(errorInfo.componentStack);
  }

  render() {
    const { hasError, errorName, errorMessage } = this.state;
    const { children } = this.props;

    return hasError ? (
      <ErrorContainer name={errorName} message={errorMessage} />
    ) : (
      children
    );
  }
}

export default ErrorBoundary;
