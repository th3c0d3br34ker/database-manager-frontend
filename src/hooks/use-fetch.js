import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// import { useLocalStorage } from 'react-use';

import instance from '../helpers/axiosInstance';
import useLocalStorage from './use-localstorage';

const useFetch = (url, key, refresh = false) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  const [data, setData, remove] = useLocalStorage(key, false);

  useEffect(() => {
    if (!url) {
      setError('Please Provide a Uri!');
      return;
    }

    if (!key) {
      setError('Please Provide a Key!');
      return;
    }

    if (refresh) {
      setData(false);
    }

    const fetchData = async () => {
      try {
        const response = await instance({
          method: 'GET',
          url,
        });

        if (response.data.success) {
          setData(response.data.data);
        } else {
          setError(response.data.message);
        }
        setLoading(false);
      } catch (err) {
        setError(err.message);
        setLoading(false);
      }
    };

    fetchData();

    // eslint-disable-next-line
  }, []);

  return { loading, data, error, remove };
};

export default useFetch;

useFetch.propTypes = {
  url: PropTypes.string.isRequired,
  key: PropTypes.string.isRequired,
  refresh: PropTypes.string,
};
