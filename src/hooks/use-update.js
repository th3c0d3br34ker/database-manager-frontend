import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import instance from '../helpers/axiosInstance';

const useUpdate = (url, id, body) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  const [data, setData] = useState({});

  useEffect(() => {
    if (!url) return;

    try {
      const fetchData = async () => {
        const response = await instance({
          method: 'POST',
          url: `${url}/${id}`,
          data: body,
        });

        if (response.data.success) {
          setData(response.data.video);
        } else {
          setError(response.data.message);
        }
        setLoading(false);
      };

      fetchData();
    } catch (err) {
      setError(err.message);
      setLoading(false);
    }
    // eslint-disable-next-line
  }, []);

  return { loading, data, error };
};

useUpdate.prototype = {
  url: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
};

export default useUpdate;
